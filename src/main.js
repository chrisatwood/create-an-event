// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import store from './storage/store';
import VeeValidate from 'vee-validate';
import Rules from './validation/Rules.js';

import App from './App';
import Squiz from './helpers/Squiz';
import Header from './components/Header';
import router from './router';
import metadata from '../data/metadata.json';
import buildings from '../data/buildings.json';

Vue.use(VeeValidate);

Vue.config.productionTip = false;

/* Core components */

const core = new Vue({
  el: '#content',
  store,
  router,
  components: {
    App
  },
  data() {
    return {
      contactEmail: 'web@cardiff.ac.uk',
      name: 'Create <br> event',

      js_api: null,
      ees_loaded: false
    };
  },
  created() {
    this.createInstance();
    this.fetchMetadata(this);
  },
  methods: {
    createInstance: () => {
     /*  this.js_api = Squiz.createInstance(); */
      this.ees_loaded = true;
      /* if (this.js_api.hasError !== undefined) {
        store.commit('updateErrorStatus', true);
        /* router.push({
          name: 'ErrorPage',
          params: { message: this.js_api.messages.friendly.replace(/ /g, '-') },
          props: {advanceMessage: this.js_api.messages.advance}
        });
      } */
    },
    fetchMetadata: (self) => {
      if (!store.getters.hasError) {
        /* new Promise((resolve, reject) => {
          Squiz.getMetadataSchema()
            .then(() => {
              console.log('loaded');
            })
            .catch(error => {
              store.commit('updateErrorStatus', true);
              /* router.push({
              name: 'ErrorPage',
              params: { message: error.friendly.replace(/ /g, '-') },
              props: {advanceMessage: error.advance}
            });
            });
        }); */

        self.loadSquizJson(
          'people',
          metadata[store.getters.assetidForOptions( 'metadata', 'people')].select_options.value
        );
        self.loadSquizJson(
          'department',
          metadata[store.getters.assetidForOptions( 'metadata', 'department')].select_options.value,
          2
        );
        self.loadSquizJson(
          'type',
          metadata[store.getters.assetidForOptions( 'metadata', 'type')].select_options.value
        );
        self.loadSquizJson(
          'topic',
          metadata[store.getters.assetidForOptions( 'metadata', 'topic')].select_options.value
        );
        self.loadSquizJson(
          'series',
          metadata[store.getters.assetidForOptions( 'metadata', 'series')].select_options.value
        );

        if (!store.getters.optionLoaded('buildings')) {
          let json = buildings.buildings;
          json.unshift({
            key: '-- Please select a building --',
            value: 'null',
            selected: true
          });

          store.commit('addOptions', {
            name: 'buildings',
            index: null,
            options: json
          });

          store.commit('optionLoadStatus', {
            name: 'building',
            status: true
          });
        }
      }
    },
    loadSquizJson: function(name, string, columnCount) {
      columnCount = columnCount || 2;
      const raw = string.split(';');
      let currentColumn = 0;
      if (!this.$store.getters.optionLoaded(name)) {
        for (var i = 0; i < raw.length - 1; i += 2) {
          if (i > 0 && (i % Math.floor(raw.length / 2) == 0 || i % Math.floor(raw.length / 2) == 1)) {
            currentColumn++;
          }
          if ((raw[i + 1].match(/\"/g) || []).length >= 2) {
            const options = {
              key: raw[i].substring(
                raw[i].indexOf('"') + 1,
                raw[i].lastIndexOf('"')
              ),
              value: raw[i + 1].substring(
                raw[i + 1].indexOf('"') + 1,
                raw[i + 1].lastIndexOf('"')
              ),
              checked: false
            };

            this.$store.commit('addOptions', {
              name: name,
              index: currentColumn,
              options: options
            });
          }
        }
        this.$store.commit('optionLoadStatus', {
          name: name,
          status: true
        });
      }
    }
  },
  template: '<App/>'
});

const header = new Vue({
  el: '#header',
  router,
  components: {
    Header
  },
  data() {
    return {
      name: 'Create <br> event'
    };
  },
  template: '<Header/>'
});
