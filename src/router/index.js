import Vue from 'vue';
import Router from 'vue-router';
import EditMode from '@/components/EditMode';
import ErrorState from '@/components/Error';
import State2 from '@/components/states/State2';
import State3 from '@/components/states/State3';
import State4 from '@/components/states/State4';
import State5 from '@/components/states/State5';
import State6 from '@/components/states/State6';
import State7 from '@/components/states/State7';
import State8 from '@/components/states/State8';
import State9 from '@/components/states/State9';
import State10 from '@/components/states/State10';
import State11 from '@/components/states/State11';
import State12 from '@/components/states/State12';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/error/:message',
      name: "ErrorPage",
      component: ErrorState,
      props: true
    },
    { 
      path: '/edit/:assetid',
      name: 'EditPage',
      component: EditMode
    },
    {
      path: '/',
      name: 'State1',
      component: State2
    },
    {
      path: '/event-details',
      name: 'State2',
      component: State2
    },
    {
      path: '/when',
      name: 'State3',
      component: State3
    },
    {
      path: '/where',
      name: 'State4',
      component: State4
    },
    {
      path: '/booking',
      name: 'State5',
      component: State5
    },
    {
      path: '/contact',
      name: 'State6',
      component: State6
    },
    {
      path: '/people',
      name: 'State7',
      component: State7
    },
    {
      path: '/department',
      name: 'State8',
      component: State8
    },
    {
      path: '/type',
      name: 'State9',
      component: State9
    },
    {
      path: '/topic',
      name: 'State10',
      component: State10
    },
    {
      path: '/series',
      name: 'State11',
      component: State11
    },
    {
      path: '/tags',
      name: 'State12',
      component: State12
    },
  ]
});
