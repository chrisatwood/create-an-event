import Vue from 'vue';
import TestUrl from './rules/test';
import { Validator } from 'vee-validate';

Validator.extend(TestUrl.name, {
  getMessage: TestUrl.message,
  validate: TestUrl.validation
});


export default {}