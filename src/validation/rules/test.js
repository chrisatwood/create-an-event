export default {
  name: 'test-less-than-5',
  message: field => `The number must be less than five)`,
  validation: value => value < 5
}