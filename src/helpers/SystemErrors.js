const get = (name, assetid) => {
  assetid = (assetid || '');


  const errors = {
    eesNotLoaded: {
      friendly: `Service is currently unavailable.`,
      advance: 'ees.js could not be loaded. Please see network log.'
    },
    noAssetid: {
      friendly: `Service is currently unavailable.`,
      advance: 'Asset id was not provided to getMetadataSchema()'
    },
    noPermissions: {
      friendly: `You currently do not have permission to use this application`,
      advance: `Current user was not able to gain permission to fetch the metadata of #${assetid}`
    },
    fetchMetadata: {
      friendly: `You currently do not have permission to use this application`,
      advance: `Error occured whilst fetching metadata for #${assetid}`
    },
    createError: {
      friendly: `The saving process has failed.`,
      advance: `Error occured whilst saving creating a new asset.`
    }

  }

  return errors[name];
}

export default {
  get
}