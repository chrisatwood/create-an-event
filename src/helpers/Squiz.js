import store from '../storage/store.js';
import Errors from './SystemErrors.js';

const createInstance = () => {
  if (typeof Squiz_Matrix_API === 'function') {
    return new Squiz_Matrix_API({ key: '1498929630' });
  } else {
    return { hasError: true, messages: Errors.get('eesNotLoaded') };
  }
};
const save = () => {
  if (store.getters.isFormCompleted()) {
    /* Create asset */
    const create = createAsset(store.getters.assetidForSave);
    /* Or edit asset */

    /* Save/ edit metadata */
    create.then(  data => {
      store.commit('addAssetid', data.assetid);
      addMetadata();
    });

    /* Create links */

    /* Create image */

    create.catch(error => {
      store.commit('updateErrorStatus', true);
      router.push({
        name: 'ErrorPage',
        params: { message: error.messages.friendly.replace(/ /g, '-') },
        props: {advanceMessage: error.messages.advance}
      });
    });
  }
};

const addMetadata = () => {
  js_api.setMetadataAllFields( {
    asset_id: assetid,
    field_info: newMetadataValues,
    dataCallback: function( d ) {
      if ( d.success !== undefined ) {
        createLink( assetid );
      }
    }
  } );
}

const update = () => {
  if (store.getters.isEditMode()) {
    const currentAsset = store.getters.assetid();
  }
};

const createAsset = (assetid) => {
  return new Promise((fulfill, reject) => {
    js_api.createAsset({
      parent_id: assetid,
      type_code: store.getters.config.assets.type,
      asset_name: store.getters.createValues.name,
      link_type: store.getters.config.linkType,
      link_value: store.getters.config.asset.linkText,
      sort_order: 0,
      is_dependant: 0,
      is_exclusive: 0,
      extra_attributes: 1,
      attributes: `description=${store.getters.createValues.description}
        &start_date=${store.getters.createValues.startDate}
        &end_date=${store.getters.createValues.endDate}`,
      dataCallback: function(createResponse) {
        if (createResponse.errorCode === undefined) {
          fulfill({ hasError: false, assetid: createResponse.id });
        } else {
          reject({ hasError: true, messages: Error.get('createError') });
        }
      }
    });
  });
};

const getMetadataSchema = assetid => {
  return new Promise((fulfill, reject) => {
    if (assetid !== undefined) {
      js_api.getPermissions({
        asset_id: assetid,
        level: '3',
        dataCallback: function(permissions) {
          if (permissions.granted !== undefined) {
            js_api.getMetadataSchema({
              assetid: assetid,
              granted: 1,
              cascades: true,
              dataCallback: function(metadata) {
                if (metadata[assetid] !== undefined) {
                  fulfill(
                    store.commit('addMetadataDefault', metadata[assetid])
                  );
                } else {
                  reject(Errors.get('fetchMetadata', assetid));
                }
              }
            });
          } else {
            reject(Errors.get('noPermissions', assetid));
          }
        }
      });
    } else {
      reject(Errors.get('noAssetid'));
    }
  });
};

export default {
  createInstance,
  getMetadataSchema
};
