import store from '../storage/store.js';
import Validate from 'vee-validate';
import router from '../router';

const next = (to) => {
  update(to);
 
  router.push({ name: `State${to}`});
};
const validate = () => {
  console.log(Validate);
}
const back = (to) => {
  update(to, true);
 
  router.push({ name: `State${to}`});
};

const update = (to, isBack) => {
  isBack = (isBack || false);
  const current = (router.history.current.name.indexOf('State') != -1 ? parseInt(router.history.current.name.replace('State', '')) : 1);

  let states = {
    next:{
      name: to,
      value: ( isBack ? ( store.getters.state(current) == 2 ?  3 : 2 ) : (store.getters.state(current)  == 2 ?  3: 1 )  )
    },
    previous: {
      name: current,
      value: (isBack ? (store.getters.state(current)  == 3 ? 2: 1): 1)
    }
  }

  if( store.getters.state(to.name) == 2) {
    nextState.value = 3;
  }
  store.commit( 'updateState', states );
}


export default {
  next,back, update, validate
}
