const mutations= {
  /**
   * Updates form input
   * @param {Object} state The current state
   * @param {String} element The variable name to change teh value of
   * @param {String} value The value to change
   */
  updateValue(state, element) {
    state.fields[element.name] = element.value;
  },

  /**
   * Update the current state and previous state and updates the status used for progress
   * @param {Object} state The current state
   * @param {Object} from Holds the name of the previous stage and the value to update
   * @param {Object} to Holds the name of the next stage and the value to update
   */
  updateState(state, states) {
    state.states[states.next.name] = states.next.value;
    state.states[states.previous.name] = states.previous.value;

    state.currentState = states.next.name;

    if (states.previous.name != 1) {
      state.previousState = states.previous.name;
    }
  },
  /**
   * Add options for checkboxes, radio, select etc to the state.
   * @param {Object} state The current state
   * @param {Object} payload
   * @property {String} payload.name The name of the option to add options too
   * @property {number} payload.index The index to add the options too
   * @property {object} payload.options The options to add to the state
   */
  addOptions(state, payload) {
    if (state.options[payload.name] !== undefined) {
      if (
        payload.index != null &&
        state.options[payload.name][payload.index] != undefined
      ) {
        state.options[payload.name][payload.index].push(payload.options);
      } else {
        state.options[payload.name][0] = payload.options;
      }
    }
  },

  /**
   * Update the load status of a option
   * @param {Object} state The current state
   * @param {Object} payload
   * @property {String} payload.name The name of the option to change
   * @property {Boolean} payload.status The value to update the option
   */
  optionLoadStatus(state, payload) {
    if (state.loaded[payload.name] !== undefined) {
      state.loaded[payload.name] = payload.status;
    }
  },

  /**
   * Set the default metadata for the asset.
   * @param {Object} state The current state
   * @param {object} metadata The metadata to set
   * @return {Boolean} true
   */
  addMetadataDefault(state, metadata) {
    state.metadtaDefault.push(metadata);
    return true;
  },

  /**
   * Set the current assetid (used for edit mode and during save process)
   * @param {Object} state The current state
   * @param {Number} assetid The assetid to set
   * @return {Boolean} true
   */
  addAssetid(state, assetid) {
    state.assetid = assetid;
    return true;
  },

  /**
   * Update the error flag.
   * @param {Object} state The current state
   * @param {Boolean} status
   */
  updateErrorStatus(state, status) {
    state.hasError = status;
  },

  formatDates(state) {
    /* Format 12h time to 24h time */
    if (state.fields.startTime == 'PM') {
      if (parseInt(hour) == 12) {
        state.fields.formatted.startHour = '00';
      } else {
        state.fields.formatted.startHour =
          parseInt(state.fields.startHour) + 12;
      }
    }

    if (state.fields.endTime == 'PM') {
      if (parseInt(hour) == 12) {
        state.fields.formatted.endHour = '00';
      } else {
        state.fields.formatted.endHour = parseInt(state.fields.endHour) + 12;
      }
    }

    /* Format start hour if only enter 0-9 */
    if (state.fields.startHour.length == 1) {
      state.fields.formatted.startHour = `0${state.fields.startHour}`;
    }
    if (state.fields.endHours.length == 1) {
      state.fields.formatted.endHours = `0${state.fields.endHours}`;
    }

    /* Format start date if only enter 1-9 */
    if (state.fields.startDate.length == 1) {
      state.fields.formatted.startDate = `0${date}`;
    }
    if (state.fields.endDate.length == 1) {
      state.fields.formatted.date = `0${date}`;
    }

    /* Format start minutes if only enter 0-9 */
    if (state.fields.startMinutes.length == 1) {
      state.fields.formatted.startMinutes = `0${state.fields.startMinutes}`;
    }

    if (state.fields.endMinutes.length == 1) {
      state.fields.formatted.endMinutes = `0${state.fields.endMinutes}`;
    }

    state.startDateString = `${state.fields.startYear}-${
      state.fields.startMonth
    }-${state.fields.formatted.startDate} ${
      state.fields.formatted.startHours
    }:${state.fields.formatted.startMinutes}:00`;

    state.endDateString = `${state.fields.endYear}-${state.fields.endMonth}-${
      state.fields.formatted.endDate
    } ${state.fields.formatted.endHours}:${
      state.fields.formatted.endMinutes
    }:00`;
  }
};

export default {
  mutations
}