const getters = {
  currentState: state => state.currentState,
  previousState: state => state.previousState,
  options: state => name => state.options[name],
  state: state => name => state.states[`State${name}`],
  optionLoaded: state => name => state.loaded[name],
  thumbnailImage: state => state.fields['thumbnailImage'],
  isFormCompleted: state => {
    return state.states.every((element, index, array) => {
      return element >= 2;
    });
  },
  isEditMode: state => state.mode == 'edit',
  isCreateMode: state => state.mode == 'create',
  assetid: state => state.assetid,
  hasError: state => state.hasError,
  publishLocation: state => state.publishLocation,
  assetidForSave: state => {
    if (state.publishLocation.contains('public')) {
      return state.assetid.assets.publish.public;
    }
    if (state.publishLocation.contains('staff')) {
      return state.assetid.assets.publish.staff;
    }
    if (state.publishLocation.contains('student')) {
      return state.assetid.assets.publish.student;
    }
  },
  assetidForOptions: state => (name, index) =>
    state.options[payload.name][payload.index],
  config: state => state.config,
  createValues: state => {
    return {
      name: state.fields.name,
      description: state.fields.description,
      startDate: state.fields.startDateString,
      endDate: state.fields.endDateString
    };
  },
  saveMetadata: state => {
    let metadata = {};

    for (const [assetid, key] of state.assetid.fields) {
      metadata[assetid] = store.fields[key];
    }

    return metadata;
  }
};

export default {
  getters
}