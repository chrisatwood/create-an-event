const state = {
  config: {
    assets: {
      type: 'calendar_event_single',
      linkType: 1,
      linkText: 'link text'
    }
  },
  states: {
    1: 1,
    2: 1,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0,
    9: 0,
    10: 0,
    11: 0,
    12: 0
  },
  currentState: 1,
  previousState: 1,
  fields: {
    name: '',
    description: '',
    startYear: new Date().getFullYear(),
    startDate: new Date().getDate(),
    startMonth: new Date().getMonth() + 1,
    startHours: new Date().getHours(),
    startMinutes: new Date().getMinutes(),
    startTime: new Date().getTime(),
    startDateString: '',
    endYear: new Date().getFullYear(),
    endDate: new Date().getDate(),
    endMonth: new Date().getMonth() + 1,
    endHours: new Date().getHours(),
    endMinutes: new Date().getMinutes(),
    endTime: new Date().getTime(),
    endDateString: '',
    teaser: '',
    hashtag: '',
    eventLanguage: [],
    contactNumber: '',
    contactName: '',
    contactEmail: '',
    bookingRequired: '',
    bookingUrl: '',
    bookingMessage: '',
    locationOnsite: '',
    locationRoom: '',
    externalAddress: '',
    externalAddressSecond: '',
    externalAdressCity: '',
    externalAdressRegion: '',
    externalAdressPostcode: '',
    people: [],
    department: [],
    type: [],
    topic: [],
    series: [],
    tags: '',
    publicAvailabilty: '',
    publishLocation: [],
    eventType: '',
    onlineEvent: '',
    onlineUrl: '',
    hasImage: false,
    thumbnailImage: '',
    thumbnailTitle: '',
    thumbnailAlt: '',
    thumbnailCaption: '',
    eventLocation: '',
    building: '',
    onlineUrl: '',
    price: '',
  
    
    formatted: {
      startDate: null,
      endDate: null,
      startHours: null,
      endHours: null,
      startMinutes: null,
      endMinutes: null
    }
  },
  options: {
    people: {
      0: [],
      1: []
    },
    type: {
      0: [],
      1: []
    },
    topic: {
      0: [],
      1: []
    },
    series: {
      0: [],
      1: [],
      2: []
    },
    department: {
      0: [],
      1: []
    },
    eventLanguage: {
      0: [
        { key: 'English', value: 'English' },
        { key: 'Welsh', value: 'Welsh' }
      ]
    },
    publishLocation: {
      0: [
        { key: 'Main website', value: 'public' },
        { key: 'Staff intranet', value: 'staff' },
        { key: 'Student intranet', value: 'student' }
      ]
    },
    eventLocation: {
      0: [
        { key: 'Internal', value: 'internal' },
        { key: 'External', value: 'external' },
        { key: 'Online', value: 'online' }
      ]
    },
    buildings: {
      0: []
    }
  },
  loaded: {
    people: false,
    type: false,
    topic: false,
    series: false,
    department: false,
    building: false
  },
  metadtaDefault: [],
  mode: 'create',
  assetid: null,
  hasError: false,
  assetid: {
    metadata: {

      people: 994684,
      department: 994688,
      type: 994689,
      topic: 994690,
      series: 945347,
      tags: 888679,
      public_availabilty: 888638,
      onlineEvent: 945337,
      onlineUrl: 985291,
      teaser: 888614,
      hashtag: 888621,
      eventLanguage: 895631,
      price: 888624,
      locationOnsite: 888675,
      locationRoom: 895631,
      externaAddress: 956256,
      externaAddressSecond: 956257,
      externaAddressCity: 956258,
      externaAddressRegion: 975435,
      externaAddressPostcode: 956259,
      bookingRequired: 945338,
      bookingUrl: 888631,
      bookingMessage: 895630,
      contactName: 944949,
      contactNumber: 944951,
      contactEmail: 944950,
      publicAvailabilty: 888638
    },
    assets: {
      type: 'calendar_event_single',
      publish: {
        public: 888626,
        staff: 946435,
        student: 947797
      },

      image_type: 'image',
      images: 988772,
      metadataInherit: 1027591,
      metadataSchema: 888589
    }
  }
};
export default{
  state
}