import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localStorage
});

const state = {
  config: {
    assets: {
      type: 'calendar_event_single',
      linkType: 1,
      linkText: 'link text'
    }
  },
  states: {
    1: 1,
    2: 1,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0,
    9: 0,
    10: 0,
    11: 0,
    12: 0
  },
  currentState: 1,
  previousState: 1,
  fields: {
    name: '',
    description: '',
    startYear: new Date().getFullYear(),
    startDate: new Date().getDate(),
    startMonth: new Date().getMonth() + 1,
    startHours: new Date().getHours(),
    startMinutes: new Date().getMinutes(),
    startTime: new Date().getTime(),
    startDateString: '',
    endYear: new Date().getFullYear(),
    endDate: new Date().getDate(),
    endMonth: new Date().getMonth() + 1,
    endHours: new Date().getHours(),
    endMinutes: new Date().getMinutes(),
    endTime: new Date().getTime(),
    endDateString: '',
    teaser: '',
    hashtag: '',
    eventLanguage: [],
    contactNumber: '',
    contactName: '',
    contactEmail: '',
    bookingRequired: '',
    bookingUrl: '',
    bookingMessage: '',
    eventFree: '',
    price: '',
    locationOnsite: '',
    locationRoom: '',
    externalAddress: '',
    externalAddressSecond: '',
    externalAddressCity: '',
    externalAddressRegion: '',
    externalAddressPostcode: '',
    people: [],
    department: [],
    departmentRequired: '',
    type: [],
    typeRequired: '',
    topic: [],
    topicRequired: '',
    series: [],
    seriesRequired: '',
    tags: '',
    tag1: '',
    tag2: '',
    tag3: '',
    searchable: '',
    publishLocation: [],
    eventType: '',
    onlineEvent: '',
    onlineUrl: '',
    hasImage: false,
    thumbnailImage: '',
    thumbnailTitle: '',
    thumbnailAlt: '',
    thumbnailCaption: '',
    eventLocation: '',
    building: '',
    onlineUrl: '',

    formatted: {
      startDate: null,
      endDate: null,
      startHours: null,
      endHours: null,
      startMinutes: null,
      endMinutes: null
    }
  },
  options: {
    people: {
      0: [],
      1: []
    },
    type: {
      0: [],
      1: []
    },
    topic: {
      0: [],
      1: []
    },
    series: {
      0: [],
      1: []
    },
    department: {
      0: [],
      1: []
    },
    eventLanguage: {
      0: [
        { key: 'English', value: 'English' },
        { key: 'Welsh', value: 'Welsh' }
      ]
    },
    publishLocation: {
      0: [
        { key: 'Public website', value: 'public' },
        { key: 'Staff intranet', value: 'staff' },
        { key: 'Student intranet', value: 'student' }
      ]
    },
    eventLocation: {
      0: [
        { key: 'Internal', value: 'internal' },
        { key: 'External', value: 'external' },
        { key: 'Online', value: 'online' }
      ]
    },
    bookingRequired: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    eventFree: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    searchable: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    departmentRequired: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    typeRequired: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    topicRequired: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    seriesRequired: {
      0: [{ key: 'Yes', value: 'Yes' }, { key: 'No', value: 'No' }]
    },
    buildings: {
      0: []
    }
  },
  loaded: {
    people: false,
    type: false,
    topic: false,
    series: false,
    department: false,
    building: false
  },
  metadataDefault: [],
  mode: 'create',
  assetid: null,
  hasError: false,
  assetid: {
    metadata: {
      people: 994684,
      department: 994688,
      type: 994689,
      topic: 994690,
      series: 945347,
      tags: 888679,
      searchable: 888638,
      onlineEvent: 945337,
      onlineUrl: 985291,
      teaser: 888614,
      hashtag: 888621,
      eventLanguage: 895631,
      price: 888624,
      locationOnsite: 888675,
      locationRoom: 895631,
      externalAddress: 956256,
      externalAddressSecond: 956257,
      externalAddressCity: 956258,
      externalAddressRegion: 975435,
      externalAddressPostcode: 956259,
      bookingRequired: 945338,
      bookingUrl: 888631,
      bookingMessage: 895630,
      contactName: 944949,
      contactNumber: 944951,
      contactEmail: 944950,
      publicAvailability: 888638
    },
    assets: {
      type: 'calendar_event_single',
      publish: {
        public: 888626,
        staff: 946435,
        student: 947797
      },

      image_type: 'image',
      images: 988772,
      metadataInherit: 1027591,
      metadataSchema: 888589
    }
  },
  availableValidationTypes: {
    alpha: 'boolean',
    alpha_dash: 'boolean',
    alpha_num: 'boolean',
    alpha_spaces: 'boolean',
    between: 'array',
    email: 'boolean',
    in: 'array',
    max: 'number',
    max_value: 'number',
    required: 'boolean',
    min: 'number',
    min_value: 'number',
    not_in: 'array',
    numeric: 'array',
    url: ['boolean', 'object'],
    message: 'string',    
    regex: 'object'
  }
};

/**
 * Handles all state changes
 *
 *
 */
const mutations = {
  /**
   * Updates form input
   * @param {Object} state The current state
   * @param {String} element The variable name to change teh value of
   * @param {String} value The value to change
   */
  updateValue(state, element) {
    state.fields[element.name] = element.value;
  },

  /**
   * Update the current state and previous state and updates the status used for progress
   * @param {Object} state The current state
   * @param {Object} from Holds the name of the previous stage and the value to update
   * @param {Object} to Holds the name of the next stage and the value to update
   */
  updateState(state, states) {
    state.states[states.next.name] = states.next.value;
    state.states[states.previous.name] = states.previous.value;

    state.currentState = states.next.name;

    if (states.previous.name != 1) {
      state.previousState = states.previous.name;
    }
  },
  /**
   * Add options for checkboxes, radio, select etc to the state.
   * @param {Object} state The current state
   * @param {Object} payload
   * @property {String} payload.name The name of the option to add options too
   * @property {number} payload.index The index to add the options too
   * @property {object} payload.options The options to add to the state
   */
  addOptions(state, payload) {
    if (state.options[payload.name] !== undefined) {
      if (
        payload.index != null &&
        state.options[payload.name][payload.index] != undefined
      ) {
        state.options[payload.name][payload.index].push(payload.options);
      } else {
        state.options[payload.name][0] = payload.options;
      }
    }
  },

  /**
   * Update the load status of a option
   * @param {Object} state The current state
   * @param {Object} payload
   * @property {String} payload.name The name of the option to change
   * @property {Boolean} payload.status The value to update the option
   */
  optionLoadStatus(state, payload) {
    if (state.loaded[payload.name] !== undefined) {
      state.loaded[payload.name] = payload.status;
    }
  },

  /**
   * Set the default metadata for the asset.
   * @param {Object} state The current state
   * @param {object} metadata The metadata to set
   * @return {Boolean} true
   */
  addMetadataDefault(state, metadata) {
    state.metadtaDefault.push(metadata);
    return true;
  },

  /**
   * Set the current assetid (used for edit mode and during save process)
   * @param {Object} state The current state
   * @param {Number} assetid The assetid to set
   * @return {Boolean} true
   */
  addAssetid(state, assetid) {
    state.assetid = assetid;
    return true;
  },

  /**
   * Update the error flag.
   * @param {Object} state The current state
   * @param {Boolean} status
   */
  updateErrorStatus(state, status) {
    state.hasError = status;
  },

  /**
   * This function is called to format a date into it's correct string format to save
   * @param {Object} state the current state
   */
  formatDates(state) {
    /* Format 12h time to 24h time */
    if (state.fields.startTime == 'PM') {
      if (parseInt(hour) == 12) {
        state.fields.formatted.startHour = '00';
      } else {
        state.fields.formatted.startHour =
          parseInt(state.fields.startHour) + 12;
      }
    }

    if (state.fields.endTime == 'PM') {
      if (parseInt(hour) == 12) {
        state.fields.formatted.endHour = '00';
      } else {
        state.fields.formatted.endHour = parseInt(state.fields.endHour) + 12;
      }
    }

    /* Format start hour if only enter 0-9 */
    if (state.fields.startHour.length == 1) {
      state.fields.formatted.startHour = `0${state.fields.startHour}`;
    }
    if (state.fields.endHours.length == 1) {
      state.fields.formatted.endHours = `0${state.fields.endHours}`;
    }

    /* Format start date if only enter 1-9 */
    if (state.fields.startDate.length == 1) {
      state.fields.formatted.startDate = `0${date}`;
    }
    if (state.fields.endDate.length == 1) {
      state.fields.formatted.date = `0${date}`;
    }

    /* Format start minutes if only enter 0-9 */
    if (state.fields.startMinutes.length == 1) {
      state.fields.formatted.startMinutes = `0${state.fields.startMinutes}`;
    }

    if (state.fields.endMinutes.length == 1) {
      state.fields.formatted.endMinutes = `0${state.fields.endMinutes}`;
    }

    state.startDateString = `${state.fields.startYear}-${
      state.fields.startMonth
    }-${state.fields.formatted.startDate} ${
      state.fields.formatted.startHours
    }:${state.fields.formatted.startMinutes}:00`;

    state.endDateString = `${state.fields.endYear}-${state.fields.endMonth}-${
      state.fields.formatted.endDate
    } ${state.fields.formatted.endHours}:${
      state.fields.formatted.endMinutes
    }:00`;
  }
};

/**
 * Handles all getters
 */
const getters = {
  currentState: state => state.currentState,
  previousState: state => state.previousState,
  options: state => name => state.options[name],
  state: state => name => state.states[`State${name}`],
  optionLoaded: state => name => state.loaded[name],
  thumbnailImage: state => state.fields['thumbnailImage'],
  isFormCompleted: state => false,
  isEditMode: state => state.mode == 'edit',
  isCreateMode: state => state.mode == 'create',
  assetid: state => state.assetid,
  hasError: state => state.hasError,
  publishLocation: state => state.publishLocation,
  assetidForSave: state => {
    if (state.fields.publishLocation.indexOf('public') != -1) {
      return state.assetid.assets.publish.public;
    }
    if (state.fields.publishLocation.indexOf('staff') != -1) {
      return state.assetid.assets.publish.staff;
    }
    if (state.fields.publishLocation.indexOf('student') != -1) {
      return state.assetid.assets.publish.student;
    }
  },
  assetidForOptions: state => (name, index) => state.assetid[name][index],
  config: state => state.config,
  createValues: state => {
    return {
      name: state.fields.name,
      description: state.fields.description,
      startDate: state.fields.startDateString,
      endDate: state.fields.endDateString
    };
  },
  saveMetadata: state => {
    console.log(state.assetid.metadata);
    /* let metadata = {};
    
  
    for (const [assetid, key] of state.assetid.metadata) {
      metadata[assetid] = state.fields[key];
    }

    return metadata; */
  },
  fieldValue: state => id =>
    state.fields[id] !== undefined ? state.fields[id] : false,
  validValidationType: state => validation => {
    if (state.availableValidationTypes[Object.keys(validation)[0]] !== undefined) {
      if ( (typeof state.availableValidationTypes[Object.keys(validation)]  == 'string' && typeof Object.values(validation)[0] === state.availableValidationTypes[Object.keys(validation)] ) ||(state.availableValidationTypes[Object.keys(validation)]  === 'object' && state.availableValidationTypes[Object.keys(validation)][typeof Object.values(validation)[0]] !== undefined ))  {
        return true;
      } else {
        throw new TypeError(`Expecting type ${state.availableValidationTypes[Object.keys(validation)[0]]} instead got ${typeof Object.values(validation)[0]}. Ignored.`, 'storage/store.js', 460
        );
        return false;
      }
    } else {
      throw new TypeError(
        'Invalid validation property. Ignored. 1',
        'storage/store.js',
        466
      );
      return false;
    }
  }
};
// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  mutations,
  getters,
  plugins: [vuexLocalStorage.plugin]
});
