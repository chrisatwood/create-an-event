# Table of contents

+ [Project introduction](#markdown-header-project-introduction)
+ [Installation](#markdown-header-installation)
+ [Rules](#markdown-header-rules)
    + [Linting](#markdown-header-linting)
    + [ES6](#markdown-header-es6)
    + [Documentation](#markdown-header-documentation)
+ [States](#markdown-header-states)
    + [Adding new states](#markdown-header-adding-new-states)
    + [Default state](#markdown-header-default-state)
+ [Routing](#markdown-header-routing)
    + [Adding new route](#markdown-header-add-new-routes)
+ [Assets](#markdown-header-assets)
+ [Storing information](#markdown-header-storing-information)
    + [Pulling information from Squiz metadata](#markdown-header-pulling-information-from-squiz-metadata)
+ [Input](#markdown-header-input)
    + [Types](#markdown-header-types)
    + [Options](#markdown-header-options)
        + [Additional parameters](#markdown-header-additional-parameters)
    + [Conditional show elements](#markdown-header-conditionally-show-input-field)
    + [Examples](#markdown-header-input-examples)
        + [Text](#markdown-header-text)
        + [Number](#markdown-header-number)
        + [Email](#markdown-header-email)
        + [Select](#markdown-header-select)
        + [Multi select](#markdown-header-multi-select)
        + [Checkbox](#markdown-header-checkbox)
        + [Radio](#markdown-header-radio)
        + [WYSIWYG](#markdown-header-wysiwyg)
        + [Image upload](#markdown-header-image-upload-container)
+ [Validation](#markdown-header-input-validation)
    + [Available checks]((#markdown-header-available-checks)
        + [Examples](#markdown-header-validation-examples)
    + [Adding custom messages](#markdown-header-adding-custom-error-messages])
    + [Adding custom field names](#markdown-header-adding-custom-field-names)
    + [Adding custom checks](#markdown-header-adding-custom-checks)
+ [Getters and Mutators](#markdown-header-getters-and-mutators)

## Project introduction

This is project by Christopher Atwood for Cardiff University Digital Communication Team.  This is a side-project to identify and build a replacement for complex javascript-reliant application on the website, intranet, and extranet.

## Rules

Current rules, please follow them :)

### Linting

This project has been created to keep a consistent format across all files including `.vue` and `.js`.  With that in mind, all new code **must** pass the automated ESLint tests with no exception.  These are put in place to keep consistency across all components.

When create a new application using VueJS, `.eslintrc.js` and `.eslintrc` should be used to keep on projects the same format.

A details explanation of all ESLint errors are found within [the following repository](https://github.com/vuejs/eslint-plugin-vue/tree/master/docs/rules).

The ESLint will automatically execute during `npm run dev` and `npm run build`

### ES6

All new javascript must be written in ECMAScript 6 where possible. Babel is used to convert ES6 to ES5 on build.

### Documentation

Document everything... sorry

+ Any new or modified code **must** be documented within the `readme.md`. If the element is new, please add a new heading ensure there is a link within the table of content.
+ All code must be document where needed within the file itself (inline comments ESLint preferred method).
+ All functions should be documented using JSDocs.  required attributes are:
    + **Description**: overview what the functions does. The core actions undertaken within the function. This should be the top sentence of the JSDoc comment
    + **@param**: For each input, please add a @param following the correct format (@param {type} (variable name) (description of the expected value) )
    + **@return**: If a function returns a value
    + **@property**: If a param is an object, please defined all properties of the object following the same format as `@param` - (@property {type} (variable name).(property name) (description of the expected value) )

### Example

```javascript
/**
 * Update the load status of a option
 * @param {Object} state The current state
 * @param {Object} payload
 * @property {String} payload.name The name of the option to change
 * @property {Boolean} payload.status The value to update the option
 */

optionLoadStatus(state, payload) {
  if (state.loaded[payload.name] !== undefined) {
    state.loaded[payload.name] = payload.status;
  }
}
```

### Visual Studio Code

Visual studio code has builtin functionality for this.  This should be enabled by default, however, if not please add the following to your settings.

```javascript
"jsDocCompletion.enabled": true,
```

Editing readme should be comply with the markdown lint provided.  [Download plugin here](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint). Once downloaded, add the following to your settings.  Any errors within the readme will be highlight or displayed within problems tab in `Shift+Command+M`

```json
"markdownlint.config": {
    "MD013": false,
    "ul-indent": {
        "indent": 4
    }
},
```

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## States

Each section of your multi-paged form is separated into 'states'. Each state defines all inputs within the data property. Property name must be 'elements' which is an array of objects. Each object is a form element. [See more information on form elements here](#markdown-header-input).

States are defined using Vue.js templating.  Naming scheme is 'State' followed by the state number i.e. State1.  All states must be places within `src/components/states/`.

## Adding new states

To add a new states, decided the state number (1,2,3,4,5,6) and create a file within `src/components/states` with the name `State[x].vue`.  Replace [x] with the state number.

Once added, you must add all id(s) that is used for state elements input into `src/store.js` within `state.fields`. Add the new state to the progress tracking which is found in `src/store.js`. If the state added is not the first state, please add the following within `states`.

```javascript
[x]: 0
```

If the state added is the introduction/landing page, please add the following within `states`.

```javascript
[x]: 1
```

## Default state

```vue
<template>
  <div class="span8">
    <h2>{{ name }}</h2>
    <template v-for="(element, index ) in elements">
      <field-element v-bind:key="element.assetid" v-bind:element="element"/>
    </template>
  </div>
</template>

<script>
import FieldElement from '../../components/elements/Element.vue';

export default {
  name: 'State2',
  data() {
    return {
      name: 'State name',
      elements: []
    };
  },
  components: {
    FieldElement
  }
};
</script>
```

## Routing

The application routing is built on Vuejs route. All routes are defined within `router/index.js`.

## Adding new routes

1. Create file
    + Decided the state number i.e. (1,2,3,4,5,6)
    + Create a file within `src/components/states` with the name `State[x].vue`.  Replace [x] with the state number.
2. Add import
    + Add the following line to top of `index.js` file
      ```javascript
      import State[x] from '@/components/states/State[x]';
      ```
3. Add a new route object
    + Add the follow object to the `routes` array within index
    ```javascript
    {
      path: '/[url path location]',
      name: '[Name the state]',
      component: [Same name as import name i.e. State[x]]
    }
    ```

## Assets

All assets id are stored within `src/mains.js`. Metadata assetid(s) should be added within `assetid.metadata` whereas assetid(s) for assets should be added within `assetid.assets`.

## Storing information

All information captured or used to render checkbox, select, and radio options must defined within the `src/store.js`. For string values please add a id of your input field within `states.fields` with the default value.

```javascript
name: 'Christopher'
```

For multi select options i.e. multi select and checkboxes, please defined an array with your default values.

```javascript
people: []
```

### Pulling information from Squiz metadata

Pulling default options form a Squiz metadata scheme is done within `src/main.js`.  It is called when the application is mounted.  To add a new default options, you can call `loadValues` after metadata is loaded.

1. Decide the id
2. Add id to `states.options` within `src/store.js`
3. Add id to `states.loaded` within `src/store.js` with default value of false
4. Add the field assetid into `src/main.js` `data.assetid.metadata`
    ```javascript
    [id]: [assetid]
    ```
5. Call loadValues with parameters to store metadata values.
    + Development
      ```javascript
      this.loadValue(
        [id], // store.state.field value
        Metadata[this.asset.metadata[{id}]].select_values.values, // Get options from squiz
        2 // Number of columns
      )
      ```

    + Production
      ```javascript
      this.loadValue(
        [id], // store.state.field value
        this.assetMetadata[this.asset.metadata[{id}]].select_values.values, // Get options from squiz
        2 // Number of columns
      )
      ```
Replace [id] with the field id.  Options are stored within `$store.states.options`.

## Input

### Types

Input name | id | usage
--- | --- | ---
Text | `text` | renders a HTML5 text input
Number | `number` | renders a HTML5 number input
Email | `email` | renders a HTML5 email input
Select option | `select` | renders a HTML5 select field
Multi select option | `multi-select` | renders a HTML5 select field with multiple options selectable
Radio button | `radio` | renders a HTML5 radio inputs
Checkbox button | `checkbox` | renders a HTML5 checkbox inputs
WYSIWYG area | `WYSIWYG` | renders a tinyMCE editing area **Only one per application** Issue #2
Image upload container | `image-upload` | renders a image upload container. This field can be modified within `src/components/elements/types/Image.vue`

[Please see example here](#markdown-header-input-examples)

### Options

Input name | Parameter name | Type | Available values | Usage | Applicable type(s) | Required
--- | --- | --- | --- | --- | --- | ---
State variable reference | `id` | String | Name of the variable held within `this.$store.state.fields` | Used to make the given element 'reactive'. This will automatically store variable into local storage | all | **Required**
Adds a label to the input | `label` | String | Any text | Name of the variable held within `this.$store.state.fields` | This will add a label above the component | all | *Optional*
Placeholder text | placeholder | String | Placeholder that appears within input type text | - | text, number, email, | *Optional*
Help text area| `helpText` | String | Text to help user complete the input correctly | This adds help text below label to aid users to enter correct values | all | *Optional*
Input class | `class` | String | Any css class | Add class to input | all | *Optional*
Input type | `type` | String | [Please see all available types](#markdown-header-types) | Format the input in correct format. | all | **Required**
Addition features | `extra` | Object | Additional features described here | Allow customised features to add to an input | all | *Optional*
Input validation | `checks` | Object | All checks to validate on. See this. | Add validation to the input| all | *Optional*
Provides list of possible options | `options` | Object | All possible options the user can select | Render a list of | Checkbox, radio | *Optional*
Allow nested elements | `nested` | Object | Object of text inputs | Render input| image-upload | *Optional*

Nested element should only be used within image.  This does not inherit the same methods as non-inherited methods. Therefore, if you add functionality to `Element.vue`, the same must be added into `Image.vue`. **Post launch task to find a better way - speak to David regarding how to call parent as a component from child**

#### Additional parameters

Additional parameters are used to add additional functionality to an input.  This parameters are defined within the extra object.

Input name | Parameter name | Type | Available values | Usage | Applicable type(s)
--- | --- | --- | --- | --- | ---
State variable reference | `limit` | Int | Max length of a input | Displays a limit counter below input field | text
Inline elements | `inline` | Boolean | true, false | - | Renders the options inline instead of vertical | checkbox, radio
Required element | `required` | Boolean | true, false | - | Renders an * after labels | all

### Conditionally show input field

To show an input based on a `this.$store.state.field` value,  the show parameter will allow you defined multiple conditions on when the  input should be shown. The show parameter is a array of objects with each object is a condition.  The input will only show if all objects(conditions) are passed.

'or' conditions can be done when the comparison is made on the same value. To accomplish this, provide all possible success values within an array.

Input name | Parameter name | Type | Available values | Usage | Required
--- | --- | --- | --- | --- | ---
State variable reference | `id` | String | Name of the variable held within `this.$store.state.fields` | - | **Required**
Operator | `operator` | String | =, !=, <, <=, >, >=, contains | Set the check operator | **Required** (Default: =)
Array of values to match | `operator` | Array | Values you wish to show element | the value must at least one of the values | **Required**

#### Show Example

```javascript
{
  id: 'room',
  type: 'text',
  label: 'Room name/ number',
  class: 'span5',
  placeholder: '',
  helpText: 'Please enter a valid email address',
  extra: {
    required: true
  },
  show:[
    {
      id: 'people',
      operator: '=',
      values: ['internal']
    }
  ]
}
```

### Input Examples

Below are default for all input types.  The show parameter is show here

#### Text

```javascript
{
  id: 'name',
  type: 'text',
  label: 'Name',
  class: 'span5',
  placeholder: '',
  helpText: ''
  extra: {,
    required: true
  }
}
```

#### Number

```javascript
{
  id: 'phoneNumber',
  type: 'number',
  label: 'Phone number',
  class: 'span5',
  placeholder: '',
  helpText: 'Please enter a valid email address',
  extra: {
    required: true
  }
}
```

#### Email

```javascript
{
  id: 'emailAddress',
  type: 'email',
  label: 'Email address',
  class: 'span5',
  placeholder: '',
  helpText: 'Please enter a valid email address'
  extra: {
    required: true
  }
}
```

#### Select

```javascript
{
  id: 'language',
  type: 'select',
  label: 'Language',
  placeholder: '',
  helpText: '',
  extra: {
    required: true
  },
  option: this.$store.state.fields.language
}
```

#### Multi select

```javascript
{
  id: 'language',
  type: 'multi-select',
  label: 'Language(s)',
  placeholder: '',
  helpText: '',
  extra: {
    required: true
  },
  options: this.$store.state.fields.language
}
```

#### Checkbox

```javascript
{
  id: 'language',
  type: 'checkbox',
  label: 'Language(s)',
  placeholder: '',
  helpText: '',
  extra: {
    required: true
  },
  option: this.$store.state.fields.language
}
```

#### Radio

```javascript
{
  id: 'people',
  type: 'radio',
  label: 'People(s)',
  placeholder: '',
  helpText: '',
  extra: {
    required: true,
    inline: true
  },
  options: this.$store.state.fields.people
}
```

### WYSIWYG

```javascript
{
  id: 'description',
  type: 'WYSIWYG',
  label: 'Description',
  helpText: '',
  extra: {
    required: true,
  }
}
```

#### Image upload container

```javascript
{
  type: 'image-upload',
  label: 'If you event has an image then you can add it below',
  nested:[
    {
      label: 'Image title',
      id: 'thumbnailTitle',
      extra:{
        required: true
      }
    }
  ]
}
```

## Input validation

Form validation is handled by [Vee Validate](https://vee-validate.logaretm.com/) with a wrapper on top to produce an easier experience and added the ability to limit some validation.  Conditional validation can be doe by adding conditionValidation string to the field with following syntax.

```javascript
conditionValidation: {
  id: '<state.fields name>',
  operator: '<operator>',
  values: ['<list of values to match to validation to show>']
}
```

## Available checks

Validation name | Type | value | Description
--- | --- | --- | ---
alpha | `Boolean` | true or false | The field under validation may only contain alphabetic characters.
alpha_dash | `Boolean` | true or false | The field under validation may contain alphabetic characters, numbers, dashes or underscores.
alpha_num | `Boolean` | true or false | The field under validation may contain alphabetic characters or numbers.
alpha_spaces | `Boolean` | true or false | The field under validation may contain alphabetic characters or spaces.
between | `Array(2)` | [Number min_value, Number max_value] | The field under validation must have a numeric value bounded by a minimum value and a maximum value.
email | `Boolean` | true or false| The field under validation must be a valid email.
in | `Array(N)` | [list of all values that will pass validation | The field under validation must have a value that is in the specified list.
max | `Number` | ([0-9]+) | (-[0-9]+) | The field under validation length may not exceed the specified length.
max_value | `Number` | ([0-9]+) | (-[0-9]+) | The field under validation must be numeric value and must not be greater than the specified value.
min | `Number` | ([0-9]+) | (-[0-9]+) | The field under validation length should not be less than the specified length.
min_value | `Number` | ([0-9]+) | (-[0-9]+) | The field under validation must be numeric value and must not be less than the specified value.
not_in | `Array(N)` | [list of all values that will pass validation | The field under validation length should not have any value within the specified value. This is mostly used in select/ checkboxes validation
numeric | `Boolean` | ([0-9]+) | The field under validation must only consist of numbers.
regex | `Pattern` | RegEx pattern | The field under validation must match the specified regular expression.
required | `Boolean` | true or false | The field under validation must have a non-empty value. By default all validators pass the validation if they have "empty values" unless they are required. Those empty values are: empty strings, undefined, null.
url | `Boolean|Object` | true or false or {required_protocol:true or false} | The field under validation must be a valid url. Protocols are not required by default.  To make protocol required, please configure the validation as below.

### Validation examples

#### Only alphabetic characters

```javascript
{
  alpha: true
}
```

#### Only alphabetic characters and dashes

```javascript
{
  alpha_dash: true
}
```

#### Only alphabetic characters and numbers

```javascript
{
  alpha_num: true
}
```

#### Only alphabetic characters and spaces

If you none of the above match the string your require, please use the regex validation method.

```javascript
{
  alpha_spaces: true
}
```

#### Between two numbers

```javascript
{
  between: [0, 15]
}
```

#### Valid email

```javascript
{
  email: true
}
```

#### In/contains

```javascript
{
  in: ['option1' 'option2', 'option4']
}
```

#### Maximum length

```javascript
{
  max: 5
}
```

#### Maximum value

```javascript
{
  max_value: 15
}
```

#### Minimum length

```javascript
{
  min: 3
}
```

#### Minimum value

```javascript
{
  min_value: 0
}
```

#### Not in

```javascript
{
  not_in: ['option3']
}
```

#### Numeric

```javascript
{
  numeric: true
}
```

#### regex

```javascript
{
  regex: /(0-9)/g
}
```

#### Required

```javascript
{
  required: true
}
```

#### Url

```javascript
{
  url: true
}
```

##### URL w/ protocol

```javascript
{
  url:{
    required_protocol: true
  }
}
```

### Adding custom error messages

To add custom error message to a validation check, you can add a message property with a `string` value.

#### Example A
```javascript
{
  required: true, message: 'Provide a name'
}
```

#### Example B

```javascript
{
  url: {
    required_protocol: true
  },
  message: 'Provide a valid url'
}
```

### Adding custom field names

To change the field name to a custom name, you can add `field_name` property with a string value.
Please note, custom error message will override this property.

```javascript
{
  required: true, field_name: 'event name'
}
```

### Adding custom checks

If the wrapper does not allow the check you required, please check the [Vee Validate docs](http://vee-validate.logaretm.com/rules.html#). If the check exists, please add validation name into `storage/store.js` `fields.availableValidationTypes` with the check name as the key and the expect data type as the value.  The wrapper converts any array to a CSV format, therefore, **do not** allow string values like the following: `'1,2,3'` instead do, `[1,2,3]`.

To add a new check, please add a new JS file within `validation/rules/` using PascalCase. Its important to keep the same format and remember to import the check into the `validation/Rules.js`. Once added, please add the custom check to list of available validations within storage. Please see example below.

#### Custom validation check example

##### validation/rules/test.js

```javascript
export default {
  name: 'test-less-than-5',
  message: field => `The number must be less than five)`,
  validation: value => value < 5
}
```

##### validation/Rules.js

```javascript
import TestUrl from './rules/test';

Validator.extend(TestUrl.name, {
  getMessage: TestUrl.message,
  validate: TestUrl.validation
});
```

Once you have checked the docs, you can create a new check within the `Validation/rules/` directory. 

## Getters and Mutators

Vuex is responsible for handling the state data, all getters and setters should defined within `store.js`.  There should not be any references to state variables, instead you should either check if a getter method already exists for your requirement or create a new getter.  Getters are defined within `getters` within `store.js`.  Creating new getters **must** follow ES6 standards.

Getters are set within `/storage/getters.js`

Mutators are set within `/storage/mutators.js`

### Setting new

Input fields are automatically added to the state variables on `input or change`.  Inputs must be set up with the current attribute `v-model="$parent.updateElement"`, which should link to their `components/elements/Element.vue` or a equivalent page. (If there are any equivalent pages, they **must** inherit all functionality of `components/elements/Element.vue`).  On update, this will call the `updateValue` mutator which only updates fields within `$store.state.fields`.

If you required to update other fields within `$store.state`, please create another mutator which is defined before.

### Change state

Changing state should be done using the `helpers/Navigation.js` class.  This will handle updating the state variables keeping everything in sync.  `helpers/Navigation.js` uses the `updateState` mutator which only responsible for changing state status.

#### Available methods

Method name | Input | Output | Description
--- | --- | --- | ---
next | Int next state number | redirect user to next state | Change state variable which changes the progress and once updated will redirect from their current state (based on url lookup) to the next
back | Int next state number | redirect user to previous state | Change state variable which changes the progress and once updated will redirect from their current state (based on url lookup) to the previous
update | Int next state number | - | This set the state variable which changes te progress. This should only be called for the introduction page and for internal functions within `helpers/Navigation.js`
addOptions | Object of options, {name, options, index(optional)} | - | This sets options for a provided name. Used within `main.js`
optionLoadStatus | Object {name, status} | - | This updates the loaded status of an option
addMetadataDefault | Object | Boolean true | This will add the default metadata for the asset
addAssetid | `Number` | Boolean | Add current assetid to state. Used in saving process and edit form
updateErrorStatus | Boolean | - | Update the error flag in the state

### Adding a new mutator

This should be done if their current mutator does not have the functionality needed.

`/storage/mutators.js`

```javascript
const mutations = {
  /**
   * JSDoc comments - Required
   */
  {mutation name}(state, {any variables that you must pass as arguments }) {
    /* Perform any basic operation. No major operations should be done here. This is saving only. */

    state.{object name| string name} = value
  }
}
```

If the mutation is not used on input fields then:

```javascript
store.commit( '{mutation name}', states );
```

If not, you can add a computed function to get and set values. To use the mutator within an input, update the `v-modal` property to point to the new computed function.

```javascript
computed: {
  {function name}: {
    get() {
      return this.$store.state.{Object name | String name};
    },
    set(value) {
      this.$store.commit('{mutation name}', {Any parameters required});
    }
  }
}
```